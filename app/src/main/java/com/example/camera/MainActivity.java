package com.example.camera;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    ImageView imageView;
    private final static int REQUEST_CODE_CAMERA=0;
    private final static int REQUEST_CODE_GALERY =1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView=findViewById(R.id.imageView);
    }

    public void myClick(View view) {
        Intent cameraIntent= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent,REQUEST_CODE_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap tmpBitmap=null;

        if (requestCode==REQUEST_CODE_CAMERA&&resultCode==RESULT_OK)
        {
            tmpBitmap=(Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(tmpBitmap);
        }//if
        if (requestCode==REQUEST_CODE_GALERY&&resultCode==RESULT_OK)
        {
            try {
                tmpBitmap=MediaStore.Images.Media.getBitmap(getContentResolver(),data.getData());
                imageView.setImageBitmap(Bitmap.createScaledBitmap(tmpBitmap,imageView.getWidth(), imageView.getHeight(),false));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }//if


    }

    public void galereyaClick(View view) {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK);//тут всякие действия есть
        galleryIntent.setType("image/*");// указываем формат
        startActivityForResult(galleryIntent,REQUEST_CODE_GALERY);


    }
}